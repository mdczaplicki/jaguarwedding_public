﻿$(document).ready(function () {
    const sidenav = $('.sidenav').sidenav();
    const instance = M.Sidenav.getInstance(sidenav);
    $('.parallax').parallax();

    $('a.menu-item').click(function(e) {
        const id = this.hash;
        const nav = $('nav');
        $('html, body').animate({ scrollTop: $(id).offset().top - nav.height() }, 'slow');
        instance.close();
    });

    let owl = $('.owl-carousel');
    owl.owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            800: {
                items: 3
            },
            1280: {
                items: 5
            }
        },
        autowidth: true,
        dots: true
    });

    $('.popup-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
        }
    });
});