from main.strings import SITE_TITLE, SITE_DESCRIPTION


def strings(request):
    return {
        'SITE_TITLE': SITE_TITLE,
        'SITE_DESCRIPTION': SITE_DESCRIPTION
    }
