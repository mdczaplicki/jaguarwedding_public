from django.shortcuts import render
from django.utils.html import format_html
from django.views import View

from main.models import ImageModel, TextModel


class IndexView(View):
    @staticmethod
    def get(request):
        parallaxes = ImageModel.objects.filter(key__icontains='parallax_')
        data = {
            p.key: p.value for p in parallaxes
        }
        texts = TextModel.objects.all()
        data.update({
            t.key: format_html(t.value) for t in texts
        })
        gallery = ImageModel.objects.filter(gallery=True)
        data['gallery'] = gallery
        return render(request, 'index.html', data)
