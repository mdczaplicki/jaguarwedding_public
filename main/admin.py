from django.contrib import admin

# Register your models here.
from main.models import TextModel, ImageModel


class TextAdmin(admin.ModelAdmin):
    list_display = ('key', 'value')


class ImageAdmin(admin.ModelAdmin):
    list_display = ('key', 'gallery')


admin.site.register(TextModel, TextAdmin)
admin.site.register(ImageModel, ImageAdmin)
