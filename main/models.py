from django.db import models


class TextModel(models.Model):
    key = models.CharField(verbose_name='Klucz', max_length=191, unique=True)
    value = models.TextField(verbose_name='Tekst')

    def __str__(self):
        return self.key + ' ' + self.value

    class Meta:
        verbose_name = 'Tekst na stronie'
        verbose_name_plural = 'Teksty na stronie'


class ImageModel(models.Model):
    key = models.CharField(verbose_name='Klucz', max_length=191, unique=True)
    value = models.ImageField(verbose_name='Obrazek', upload_to='images')
    gallery = models.BooleanField(verbose_name='Galeria', help_text='Obrazek będzie znajdował się w galerii.')

    def __str__(self):
        return self.key

    class Meta:
        verbose_name = 'Obrazek na stronie'
        verbose_name_plural = 'Obrazki na stronie'
